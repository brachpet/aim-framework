Projekt predmetu AIM

Semestralni prace cislo 6 (Aplikace nelinearni filtrace: potlaceni sumu pri zachovani ostrych hran a mapovani t�n� s vysokym dynamickym rozsahem)

V souboru EntryPoint.cpp se nachazi metoda main. Odtud se pak volaji veskere operace nad obrazky. Build proveden na windows - visual studiu 2017 (64 bit).

Prace na cviceni 27.2.
	- nacitani a ukladani obrazku
	- monadicke filtry

Prace na cviceni 13.3.
	- bilateralni filtr: Image.cpp (radek 386)
	- FT: Image.cpp (radek 294)
	- vstupni soubory: budova.jpg, pointPosition1.jpg, pointPosition2.jpg
	- ukazka, ze posun bodu v obrazku nema vliv na fourieruv obraz