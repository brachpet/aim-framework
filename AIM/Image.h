#pragma once

class Image {
	float* data; /*normalized values for LDR*/
public:
	int width;
	int height;
	int comp;

private:
	void loadToFloat(unsigned char* datac);
	int c(int x, int y) const; //index prvku v datovem poli obrazku na souradnicich x,y

	inline float clamp(float x) const;
	inline float clamp(float x, float min, float max) const;

	float& a(int x, int y);	//pristup k pixelu na souradnici x,y
	float& a(int x, int y, int z); //pristup k pixelu na souradnicich x,y v definovanem kanalu z
	float ac(int x, int y) const;	//hodnota pixelu na souradnicich x,y
	float ac(int x, int y, int z) const;	//hodnota pixelu na souradnicich x,y v pozadovanem kanalu z
	float acs(int x, int y) const;	//hodnota pixelu na souradnicich x,y s kontrolou okraju (wrap arround)
	float acs(int x, int y, int z) const;	//hodnota pixelu na souradnicich x,y v pozadovanem kanalu z s kontrolou okraju (wrap arround), z: R=0, G=1, B=2, A=3

public:
	Image();
	~Image();

	Image(int width, int height, int comp);
	void copyFrom(float* other);

	void setData(float* newData);
	float* getData();

	float& operator()(int x, int y, int z);
	float& operator()(int x, int y);

	void load(const char* file);
	void loadHDR(const char* file);

	int writeTga(const char* file);
	int writePng(const char* file);
	int writeJpeg(const char* file);
	int writeHDR(const char* file);

	void normalizeHDR();
	void normalizePallete();

	void toHSL();
	void toRGB();

	// canal: 0=R, 1=G, 2=B, 3=A
	void filterBrightness(float b);
	void filterEqualization(int canal);
	void filterMapping(int canal, const Image& other);	//zmeni histogram, aby odpovidal druhemu obrazku
	void filterLog(int canal, float s);
	void filterMultiply(float b);
	void filterLaplace();

	void histogram(int canal, int* histogram) const;
	void pdf(int canal, float* pdf) const;
	void cdf(int canal, float* cdf) const;

	void FTamplitude();

	// cim vyssi edgePreservingCoef, tim vetsi respektovani hran
	// edgePreservingCoef = 0.0f => klasicky blur bez edgePreserving
	void bilateralBlur(int size, float sigma, float edgePreservingCoef);
	// size = rozmer kernelu
	void blur(int size, float sigma);
	void separableBlur(int size, float sigma);

	void convolute(float* kernel, int size, float* output);
	void convoluteHorisontal(float* kernel, int size, float* output);
	void convoluteVertical(float* kernel, int size, float* output);

	void computeIntensity(float* output);
	void computeBase(float* output);
	void divideColor(float* intensity);
	void multiplyColor(float* intensity);
	void intensityLog();
	void computeDetail(float* intensity, float* base);
	void computeCompressedIntensity(float* base, float* detail, float compressionfactor, float logAbsScale);
	void add(float* other);

	float getCompressionFactor(float targetContrast);
	float getLogAbsScale(float compFac);

	void alterLightness(float* other);

	void gradientSew(Image& other, int gaussSeidelIterationCount);
	void sew(Image& other);
	void gaussSeidel(Image& gradient, int iterationCount);

	void colourByMask(const Image& mask, Image& outputColorLayer, const float* color);

	void multiply(const Image& other);

	void resize(int w, int h);
};
