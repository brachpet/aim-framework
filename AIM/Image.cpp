#include "Image.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBI_MSC_SECURE_CRT
#include <stb_image_write.h>

#include <limits>
#include <iostream>

#include <fftw3.h>


#include <GridCut/GridGraph_2D_4C.h>

void Image::loadToFloat(unsigned char * datac)
{
	for (int i = 0; i < width*height*comp; i++) {
		data[i] = datac[i] / 255.0f;
	}
}

int Image::c(int x, int y) const
{
	if (x >= width) x = width - 1;
	if (y >= height) y = height - 1;
	if (x <= 0) x = 0;
	if (y <= 0) y = 0;
	//x %= width;
	//y %= height;
	return (y*width + x)*comp;
}

inline float Image::clamp(float x) const
{
	if (x < 0.0f) return 0.0f;
	if (x > 1.0f) return 1.0f;
	return x;
}

inline float Image::clamp(float x, float min, float max) const
{
	if (x < min) return min;
	if (x > max) return max;
	return x;
}

float & Image::a(int x, int y)
{
	return (data[c(x,y)]);
}

float & Image::a(int x, int y, int z)
{
	return (data[c(x, y) + z]); // &(*data[c(x, y)] + z);
}

float Image::ac(int x, int y) const
{
	return data[c(x, y)];
}

float Image::ac(int x, int y, int z) const
{
	return data[c(x, y) + z];
}

float Image::acs(int x, int y) const
{
	/*
	if (x < 0) x += width;
	if (y < 0) y += height;
	x = x % width;
	y = y % height;*/
	if (x >= width) x = width - 1;
	if (y >= height) y = height - 1;
	if (x <= 0) x = 0;
	if (y <= 0) y = 0;
	return data[c(x, y)];
}

float Image::acs(int x, int y, int z) const
{
	/*
	x = x % width;
	y = y % height;
	if (x < 0) x += width;
	if (y < 0) y += height; */
	if (x >= width) x = width - 1;
	if (y >= height) y = height - 1;
	if (x <= 0) x = 0;
	if (y <= 0) y = 0;
	return data[c(x, y) + z];
}

Image::Image()
{
}

Image::~Image()
{
	if (data != nullptr)
		delete[] data;
}

Image::Image(int width, int height, int comp) : width(width), height(height), comp(comp), data(new float[width*height*comp])
{

}

void Image::copyFrom(float * other)
{
	for (int i = 0; i < width*height*comp; i++)
	{
		data[i] = other[i];
	}
}

void Image::setData(float * newData)
{
	if (data != nullptr)
		delete[] data;

	data = newData;
}

float * Image::getData()
{
	return data;
}

float & Image::operator()(int x, int y, int z)
{
	return a(x, y, z);
}

float & Image::operator()(int x, int y)
{
	return a(x, y);
}

void Image::load(const char * file)
{
	unsigned char * datac = stbi_load(file, &width, &height, &comp, 0);
	if (data == nullptr)
		delete[] data;

	data = new float[width*height*comp];

	loadToFloat(datac);
	delete[] datac;
}

void Image::loadHDR(const char * file)
{
	data = stbi_loadf(file, &width, &height, &comp, 0);
}

int Image::writeTga(const char * file)
{
	unsigned char* datac = new unsigned char[width*height*comp];
	for (int i = 0; i < width*height*comp; i++)
	{
		datac[i] = (unsigned char)(data[i] * 255.0f);
	}

	int result = stbi_write_tga(file, width, height, comp, datac);

	delete[] datac;
	return result;
}

int Image::writePng(const char * file)
{
	unsigned char* datac = new unsigned char[width*height*comp];
	for (int i = 0; i < width*height*comp; i++)
	{
		datac[i] = (unsigned char)(data[i] * 255.0f);
	}

	int result = stbi_write_png(file, width, height, comp, datac, width * comp * sizeof(unsigned char));

	delete[] datac;
	return result;
}

int Image::writeJpeg(const char * file)
{
	unsigned char* datac = new unsigned char[width*height*comp];
	for (int i = 0; i < width*height*comp; i++)
	{
		datac[i] = (unsigned char)(data[i] * 255.0f);
	}

	int result = stbi_write_jpg(file, width, height, comp, datac, 80);

	delete[] datac;
	return result;
}

int Image::writeHDR(const char * file)
{
	return stbi_write_hdr(file, width, height, comp, data);
}

void Image::normalizeHDR()
{
	float max = std::numeric_limits<float>::min();

	for (int i = 0; i < width*height*comp; i++)
	{
		if (max < data[i]) max = data[i];
	}

	for (int i = 0; i < width*height*comp; i++)
	{
		data[i] = data[i] / max;
	}
}

void Image::normalizePallete()
{
	float max = std::numeric_limits<float>::min();
	float min = std::numeric_limits<float>::max();

	for (int i = 0; i < width*height*comp; i++)
	{
		if (max < data[i]) max = data[i];
		if (min > data[i]) min = data[i];
	}

	for (int i = 0; i < width*height*comp; i++)
	{
		data[i] = (data[i] - min) / (max - min);
	}
}

float max(float a, float b, float c) {
	if (a < b) {
		if (b < c)
			return c;
		else
			return b;
	}
	else {
		if (a < c)
			return c;
		else
			return a;
	}
}

float min(float a, float b, float c) {
	if (a > b) {
		if (b > c)
			return c;
		else
			return b;
	}
	else {
		if (a > c)
			return c;
		else
			return a;
	}
}

void Image::toHSL()
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j * width + i;
			float r = a(i, j, 0);
			float g = a(i, j, 1);
			float b = a(i, j, 2);

			float cmax = max(r, g, b);
			float cmin = min(r, g, b);
			float d = cmax - cmin;

			float h = 0.0f;
			float s = 0.0f;
			float l = (cmax + cmin) / 2.0f;

			if (d != 0.0f) {
				s = d / (1.0f - abs(2.0f*l - 1.0f));
				if (cmax == r) {
					float segment = (g - b) / d;
					float shift = 0.0f;       // R� / (360� / hex sides)
					if (segment < 0.0f) {          // hue > 180, full rotation
						shift = 6.0f;         // R� / (360� / hex sides)
					}
					h = segment + shift;
					h *= 60.0f;

					//h = 60.0f * ((int)((g - b) / d) % 6);
				}
				else if (cmax == g) {
					h = 60.0f * (((b - r) / d) + 2.0f);
				}
				else {
					h = 60.0f * (((r - g) / d) + 4.0f);
				}
			}

			a(i, j, 0) = h;
			a(i, j, 1) = s;
			a(i, j, 2) = l;
		}
	}
}

float htorgb(float p, float q, float t) {
	if (t < 0.0f) t += 1.0f;
	if (t > 1.0f) t -= 1.0f;
	if (t < 1.0f / 6.0f) return p + (q - p) * 6.0f * t;
	if (t < 1.0f / 2.0f) return q;
	if (t < 2.0f / 3.0f) return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
	return p;
}

void Image::toRGB()
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j * width + i;
			float h = a(i, j, 0);
			float s = a(i, j, 1);
			float l = a(i, j, 2);

			float c = (1.0f - abs(2.0f * l - 1.0f)) * s;
			float x = c * (1.0f - abs(((int)(h / 60.0f) % 2) - 1.0f));
			float m = l - c / 2.0f;

			float r;
			float g;
			float b;

			h = h / 360.0f;

			if (s == 0.0f) {
				r = l;
				g = l;
				b = l;
			} else {
				float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
				float p = 2.0f * l - q;
				r = htorgb(p, q, h + 1.0f / 3.0f);
				g = htorgb(p, q, h);
				b = htorgb(p, q, h - 1.0f / 3.0f);
			}

			a(i, j, 0) = r;
			a(i, j, 1) = g;
			a(i, j, 2) = b;
		}
	}
}

void Image::filterBrightness(float b)
{
	b = clamp(b, -1.0f, 1.0f);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			for (int k = 0; k < comp; k++)
			{
				a(x, y, k) = clamp(a(x, y, k) + b);
			}
		}
	}
}

void Image::filterEqualization(int canal)
{
	float cdfData[256];
	for (int i = 0; i < 256; i++)
		cdfData[i] = 0.0f;
	cdf(canal, cdfData);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int pix = (int)(a(x, y, canal) * 255.0f);
			a(x, y, canal) = cdfData[pix];
		}
	}

}

void Image::filterMapping(int canal, const Image & other)
{
	float cdfB[256];
	float cdfA[256];
	for (int i = 0; i < 256; i++)
	{
		cdfA[i] = 0.0f;
		cdfB[i] = 0.0f;
	}
	cdf(canal, cdfA);
	other.cdf(canal, cdfB);
	
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int i = (int)(a(x, y, canal) * 255.0f);
			int j = 0;
			while (cdfA[i] > cdfB[j])
				j++;

			a(x, y, canal) = j / 255.0f;
		}
	}
}

void Image::filterLog(int canal, float s)
{
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			float l = a(x, y, canal);

			a(x, y, canal) = logf(1.0f + l * s) / log(1 + s);
		}
	}
}

void Image::filterMultiply(float b)
{
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			for (int k = 0; k < comp; k++)
			{
				a(x, y, k) = a(x, y, k) * b;
			}
		}
	}
}

void Image::filterLaplace()
{
	float* tmp = new float[width*height*comp];

	//laplace kernel
	float kernel[] = { 0.0f, 1.0f, 0.0f,
					   1.0f,-4.0f, 1.0f,
					   0.0f, 1.0f, 0.0f };

	//konvoluce
	convolute(kernel, 3, tmp);

	delete[] data;
	data = tmp;
}

void Image::histogram(int canal, int * histogram) const
{
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int i = (int)(ac(x, y, canal) * 255.0f);
			histogram[i]++;
		}
	}
}

void Image::pdf(int canal, float * pdf) const
{
	int histogramData[256];
	for (int i = 0; i < 256; i++)
		histogramData[i] = 0;
	histogram(canal, histogramData);
	for (int i = 0; i < 256; i++)
	{
		pdf[i] = histogramData[i] / (float)(width*height);
	}
}

void Image::cdf(int canal, float * cdf) const
{
	pdf(canal, cdf);
	float sum = 0.0f;
	for (int i = 0; i < 256; i++)
	{
		sum += cdf[i];
		cdf[i] = sum;
	}
}

float ampl(fftwf_complex c) {
	return sqrtf(c[0] * c[0] + c[1] * c[1]);
}

void Image::FTamplitude()
{
	fftwf_complex *out;
	fftwf_plan p;

	float * in = new float[width * height];
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			in[j*width + i] = data[(j*width + i)*comp];
		}
	}

	out = (fftwf_complex*)fftwf_malloc(sizeof(fftwf_complex) * (((width / 2) + 1)*height));
	p = fftwf_plan_dft_r2c_2d(height, width, in, out, FFTW_ESTIMATE);
	fftwf_execute(p);

	for (int i = 0; i < ((width / 2) + 1); i++)
	{
		for (int j = 0; j < height; j++)
		{
			a(i,j) = ampl(out[j* ((width / 2) + 1) + i]);
		}
	}

	//vertical mirror copy
	for (int i = 0; i < (width / 2); i++)
	{
		for (int j = 0; j < height; j++)
		{
			a(width - i - 1, j) = a(i, j);
		}
	}

	//central quadrant flip
	int centerX = width / 2;
	int centerY = height / 2;

	for (int i = 0; i < centerX / 2; i++)
	{
		for (int j = 0; j < centerY; j++)
		{
			float tmp = ac(i, j);
			a(i, j) = ac(centerX - i - 1, centerY - j - 1);
			a(centerX - i - 1, centerY - j - 1) = tmp;

			tmp = ac(i+centerX, j);
			a(i + centerX, j) = ac(centerX - i + centerX - 1, centerY - j - 1);
			a(centerX - i + centerX - 1, centerY - j - 1) = tmp;

			tmp = ac(i, j + centerY);
			a(i, j + centerY) = ac(centerX - i - 1, centerY - j + centerY - 1);
			a(centerX - i - 1, centerY - j + centerY - 1) = tmp;

			tmp = ac(i + centerX, j + centerY);
			a(i + centerX, j + centerY) = ac(centerX - i + centerX - 1, centerY - j + centerY - 1);
			a(centerX - i + centerX - 1, centerY - j + centerY - 1) = tmp;
		}
	}

	fftwf_destroy_plan(p);
	fftwf_free(out);
	delete[] in;
}



// gauss 2D
float gauss2D(float x, float y, float sigma) {
	return expf(-(x * x + y * y) / (2 * sigma*sigma)) / (2.0 * 3.14159f * sigma * sigma);
}

// gauss 1D
float gauss1D(float x, float sigma) {
	return expf(-(x * x) / (2 * sigma*sigma)) / (sqrtf(2.0 * 3.14159f) * sigma);
}

void generateKernelGauss2D(float* kernel, int size, float sigma) {
	float sum = 0.0f;
	float step = sigma * 3.0f / (size / 2);
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			kernel[j*size + i] = gauss2D((j - (size / 2))*step, (i - (size / 2))*step, sigma);
			sum += kernel[j*size + i];
		}
	}
	for (int i = 0; i < size*size; i++)
	{
		kernel[i] /= sum;
	}
}

void generateKernelGauss1D(float* kernel, int size, float sigma) {
	float sum = 0.0f;
	float step = sigma * 3.0f / (size / 2);
	for (int i = 0; i < size; i++)
	{
		kernel[i] = gauss1D((i - (size / 2))*step, sigma);
		sum += kernel[i];
	}
	for (int i = 0; i < size; i++)
	{
		kernel[i] /= sum;
	}
}

/*
// cone
float g(float x, float y, float r) {
	float i = x - (r / 2.0f);
	float j = y - (r / 2.0f);
	return sqrtf(i * i + j * j);
}*/

/*
// box
float g(float x, float y, float r) {
	return 1.0f;
}*/

// vzdalenostni funkce pro edgePreserving. Koeficient je mocnina
float distCoef(float x, float y, float edngePreservingCoef) {

	float d = gauss1D(logf(x) - logf(y), 1.5f);
	return d;

	//return powf(1.0f - abs(x - y), edngePreservingCoef);
}

void Image::bilateralBlur(int size, float sigma, float edgePreservingCoef)
{
	float* tmp = new float[width*height*comp];

	//predpocitani kernelu
	float* kernel = new float[size * size];
	generateKernelGauss2D(kernel, size, sigma);

	//obrazek
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = c(i, j);
			float normTerm = 0.0f;
			tmp[index] = 0.0f;

			//kernel
			for (int x = 0; x < size; x++)
			{
				for (int y = 0; y < size; y++)
				{
					//adresa bunky v kernelu
					int ki = i - (size / 2) + x;
					int kj = j - (size / 2) + y;

					float coef = kernel[y*size + x] * distCoef(acs(ki, kj), acs(i, j), edgePreservingCoef);
					normTerm += coef;

					tmp[index] += acs(ki, kj) * coef;
				}
			}
			//vyvazeni sumy
			tmp[index] /= normTerm;
		}
	}

	delete[] kernel;
	delete[] data;
	data = tmp;
}

void Image::blur(int size, float sigma)
{
	float* tmp = new float[width*height*comp];

	//predpocitani kernelu
	float* kernel = new float[size * size];
	generateKernelGauss2D(kernel, size, sigma);

	//konvoluce
	convolute(kernel, size, tmp);

	delete[] kernel;
	delete[] data;
	data = tmp;
}

void Image::separableBlur(int size, float sigma)
{
	float* tmp = new float[width*height*comp];

	float* kernel = new float[size];
	generateKernelGauss1D(kernel, size, sigma);

	//horisontal pass
	convoluteHorisontal(kernel, size, tmp);

	float* sw = tmp;
	tmp = data;
	data = sw;

	//vertical pass
	convoluteVertical(kernel, size, tmp);

	delete[] kernel;
	delete[] data;
	data = tmp;
}

void Image::convolute(float * kernel, int size, float* output)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < comp; k++)
			{
				int index = c(i, j) + k;
				output[index] = 0.0f;

				for (int x = 0; x < size; x++)
				{
					for (int y = 0; y < size; y++)
					{
						int ki = i - (size / 2) + x;
						int kj = j - (size / 2) + y;

						output[index] += acs(ki, kj, k) * kernel[(y*size + x)];
					}
				}
			}
		}
	}
}

void Image::convoluteHorisontal(float * kernel, int size, float* output)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = c(i, j);
			output[index] = 0.0f;

			for (int x = 0; x < size; x++)
			{
				int ki = i - (size / 2) + x;
				output[index] += acs(ki, j) * kernel[x];
			}
		}
	}
}

void Image::convoluteVertical(float * kernel, int size, float* output)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = c(i, j);
			output[index] = 0.0f;

			for (int x = 0; x < size; x++)
			{
				int kj = j - (size / 2) + x;
				output[index] += acs(i, kj) * kernel[x];
			}
		}
	}
}

void Image::computeIntensity(float * output)
{
	toHSL();

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j*width + i;
			//float intensity = 1.0f / 61.0f * (acs(i, j, 0) * 20.0f + acs(i, j, 1) * 40.0f + acs(i, j, 2));
			output[index] = acs(i, j, 2);// intensity;
		}
	}

	toRGB();
}

void Image::computeBase(float * output)
{
	
}

void Image::multiplyColor(float * intensity)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j * width + i;
			a(i, j, 0) = a(i, j, 0) * intensity[index]; // powf(10.0f, intensity[index]);
			a(i, j, 1) = a(i, j, 1) * intensity[index]; //powf(10.0f, intensity[index]);
			a(i, j, 2) = a(i, j, 2) * intensity[index]; //powf(10.0f, intensity[index]);
		}
	}
}

void Image::intensityLog()
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			a(i, j) = logf(1.0f + a(i, j));
		}
	}
}

void Image::computeDetail(float * intensity, float * base)
{
	for (int i = 0; i < width*height; i++)
	{
		data[i] = intensity[i] - base[i];
	}
}

void Image::computeCompressedIntensity(float * base, float * detail, float compressionfactor, float logAbsScale)
{
	for (int i = 0; i < width*height; i++)
	{
		data[i] = base[i] * compressionfactor + detail[i] - logAbsScale;
	}
}

void Image::add(float * other)
{
	for (int i = 0; i < width*height; i++){
		data[i] += other[i];
	}
}

float Image::getCompressionFactor(float targetContrast)
{
	float max = std::numeric_limits<float>::min();
	float min = std::numeric_limits<float>::max();

	for (int i = 0; i < width*height; i++)
	{
		float val = data[i];
		if (val > max)
			max = val;
		if (val < min)
			min = val;
	}


	return targetContrast / (max - min);
}

float Image::getLogAbsScale(float compFac)
{
	float max = std::numeric_limits<float>::min();

	for (int i = 0; i < width*height; i++)
	{
		float val = data[i];
		if (val > max)
			max = val;
	}

	return max * compFac;
}

void Image::alterLightness(float * other)
{
	toHSL();

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j * width + i;
			//float intensity = 1.0f / 61.0f * (acs(i, j, 0) * 20.0f + acs(i, j, 1) * 40.0f + acs(i, j, 2));
			a(i, j, 2) = other[index];// intensity;
		}
	}

	toRGB();
}

void Image::gradientSew(Image & other, int gaussSeidelIterationCount)
{
	//vytvoreni kopii obou obrazku
	Image grad = Image(width, height, comp);
	grad.copyFrom(data);

	Image gradOther = Image(width, height, comp);
	gradOther.copyFrom(other.data);

	//gradicent obou kopii
	grad.filterLaplace();
	gradOther.filterLaplace();

	//sesiti gradientu obou kopii
	grad.sew(gradOther);

	//sesiti obou vstupu
	sew(other);

	//GaussSeidel
	gaussSeidel(grad, gaussSeidelIterationCount);

	//vykresleni slozeneho gradientu
	grad.filterMultiply(0.5f);
	grad.filterBrightness(0.5f);
	grad.writeJpeg("sew/gradientCheck.jpg");
}

void Image::sew(Image & other)
{
	for (int i = 300; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < comp; k++)
			{
				a(i, j, k) = other.a(i, j, k);
			}
		}
	}
}

void Image::gaussSeidel(Image & gradient, int iterationCount)
{
	Image tmp = Image(width, height, comp);
	tmp.copyFrom(data);

	for (int i = 0; i < iterationCount; i++)
	{
		for (int x = 1; x < width-1; x++)
		{
			for (int y = 1; y < height-1; y++)
			{
				for (int k = 0; k < comp; k++)
				{
					tmp.a(x, y, k) = 0.25f * ( a(x+1,y,k) + a(x-1,y,k) + a(x,y+1,k) + a(x,y-1,k) - gradient.a(x,y,k) );
				}
			}
		}
		float* dataTmp = data;
		data = tmp.data;
		tmp.data = dataTmp;
	}

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < comp; k++)
			{
				a(i, j, k) = clamp(a(i, j, k));
			}
		}
	}
}

int getCapacity(float intensityA, float intensityB, float gamma, int multiplicator) {
	float min = intensityA;
	if (intensityB < min)
		min = intensityB;
	return 1 + multiplicator * std::powf(min, gamma);

	//return 1 + multiplicator * std::powf(std::abs(intensityA - intensityB), gamma);
}

void Image::colourByMask(const Image & mask, Image& outputColorLayer, const float* color)
{
	GridGraph_2D_4C<int, int, int>* graph = new GridGraph_2D_4C<int, int, int>(width, height);

	//float gamma = 2.0f;
	//int multiplicator = 100;
	float gamma = 2.5f;
	int multiplicator = 2000;

	//setup grafu
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			/////////sink a source/////////
			graph->set_terminal_cap(graph->node_id(i, j), multiplicator * mask.acs(i, j, 1), multiplicator * mask.acs(i, j, 0));
			
			/////////sousedi/////////
			if (i < width-1)
				graph->set_neighbor_cap(graph->node_id(i, j),  1, 0, getCapacity(acs(i, j, 0), acs(i + 1, j, 0), gamma, multiplicator));
			if (i > 0)
				graph->set_neighbor_cap(graph->node_id(i, j), -1, 0, getCapacity(acs(i, j, 0), acs(i - 1, j, 0), gamma, multiplicator));
			if (j < height-1)
				graph->set_neighbor_cap(graph->node_id(i, j), 0,  1, getCapacity(acs(i, j, 0), acs(i, j + 1, 0), gamma, multiplicator));
			if (j > 0)
				graph->set_neighbor_cap(graph->node_id(i, j), 0, -1, getCapacity(acs(i, j, 0), acs(i, j - 1, 0), gamma, multiplicator));

		}
	}

	graph->compute_maxflow();

	outputColorLayer.resize(width, height);

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int segment = graph->get_segment(graph->node_id(i, j));

			for (int k = 0; k < comp; k++)
			{
				if (segment == 1) {
					outputColorLayer.a(i, j, k) = color[k];
				}
				else {
					outputColorLayer.a(i, j, k) = 1.0f;
				}
			}			
		}
	}


	delete graph;
}

void Image::multiply(const Image & other)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < comp; k++)
			{
				a(i, j, k) = a(i, j, k) * other.acs(i, j, k);
			}
		}
	}
}

void Image::resize(int w, int h)
{
	if (w == width && h == height)
		return;

	width = w;
	height = h;
	delete[] data;
	data = new float[w*h*comp];
}

void Image::divideColor(float * intensity)
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = j * width + i;
			a(i, j, 0) = a(i, j, 0) / intensity[index];
			a(i, j, 1) = a(i, j, 1) / intensity[index];
			a(i, j, 2) = a(i, j, 2) / intensity[index];
		}
	}
}
