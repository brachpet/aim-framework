#include <iostream>
#include <string>
#include <sstream>
#include "Image.h"


int main() {
	///1.
	std::cout << "jednoduchy filtr" << std::endl;
	{	
		Image img;
		img.load("budova.jpg");
		img.filterEqualization(0);
		img.writeJpeg("budovaEqualizace.jpg");
	}

	///2.
	std::cout << "FT" << std::endl;
	{
		Image img3;
		img3.load("pointPosition1.jpg");
		img3.FTamplitude();
		img3.normalizeHDR();
		img3.filterLog(0, 1000.0f);
		img3.writeJpeg("fourierPoint1.jpg");

		Image img4;
		img4.load("pointPosition2.jpg");
		img4.FTamplitude();
		img4.normalizeHDR();
		img4.filterLog(0, 1000.0f);
		img3.writeJpeg("fourierPoint2.jpg");
	}

	///3.
	std::cout << "separabilni blur" << std::endl;
	{
		Image img;
		img.load("budova.jpg");
		img.separableBlur(7, 1.0f);
		img.writeJpeg("budovaSeparable.jpg");
	}

	///4.
	std::cout << "bilateral blur" << std::endl;
	{
		Image img;
		img.load("budova.jpg");
		img.bilateralBlur(7, 1.0f, 1.0f);
		img.writeJpeg("budovaBilateral.jpg");
	}

	return 0;

	///5.
	std::cout << "image sew" << std::endl;
	{
		Image img;
		img.load("sew/input1.png");

		Image img2;
		img2.load("sew/input2.png");

		for (int i = 0; i <= 600; i += 200)
		{
			Image tmp = Image(img.width, img.height, img.comp);
			tmp.copyFrom(img.getData());
			tmp.gradientSew(img2, i);

			std::ostringstream oss;
			oss << "sew/output" << i << ".jpg";
			std::string var = oss.str();

			tmp.writeJpeg(var.c_str());
		}

	}

	///6.
	std::cout << "barveni" << std::endl;
	{	//blob
		Image img;
		img.load("barveni/kontury.png");

		Image mask;
		mask.load("barveni/maskaBarvy.png");

		Image colourLayer;
		colourLayer.load("barveni/kontury.png");

		float color[4] = { 1.0f, 0.0f, 1.0f, 1.0f};

		img.colourByMask(mask, colourLayer, color);
		img.multiply(colourLayer);
		img.writePng("barveni/output.png");
	}

	{	//jezek (by krutoprisny animator Zuzana Kalivoda Brachaczkova https://www.youtube.com/watch?v=BW9X8TJreYA&ab_channel=Aerokratas)
		Image img;
		img.load("barveni/kontury2.png");

		Image mask;
		mask.load("barveni/maskaBarvy2.png");

		Image colourLayer;
		colourLayer.load("barveni/kontury2.png");

		float color[4] = { 0.56f, 0.39f, 0.21f, 1.0f };

		img.colourByMask(mask, colourLayer, color);
		img.multiply(colourLayer);
		img.writePng("barveni/output2.png");
	}

	{	//vombat (by krutoprisny animator Zuzana Kalivoda Brachaczkova https://www.youtube.com/watch?v=BW9X8TJreYA&ab_channel=Aerokratas)
		Image img;
		img.load("barveni/kontury3.png");

		Image mask;
		mask.load("barveni/maskaBarvy3.png");

		Image colourLayer;
		colourLayer.load("barveni/kontury3.png");

		float color[4] = { 0.56f, 0.39f, 0.21f, 1.0f };

		img.colourByMask(mask, colourLayer, color);
		img.multiply(colourLayer);
		img.writePng("barveni/output3.png");
	}

	std::cout << "Done, press enter to close" << std::endl;
	std::cin.get();

	return 0;
}